import React from 'react'
import ReactDOM from "react-dom";
import UserContactForm from './pages/UserContactForm';
import { injectGlobal } from 'styled-components';

injectGlobal`
  body {
    font-family: sans-serif;
  }
  * {
    box-sizing: border-box;
    }
  .field-error .form-control{
	border-color: #b71229;
  }
  .form-group{
    padding: 0px 10px;
  }
  .label-inline .control-label{
    width: 30%;
    min-width: 100px;
    float: left;
    height: 40px;
    line-height: 35px;
  }
  .label-inline .input-group{
    width: 70%;
    min-width: calc(100% - 220px);
    float: left;
  }
`;

ReactDOM.render(<UserContactForm />, document.getElementById('root'));

// const renderApp = (AppComponent) =>
//   render(<AppComponent />, document.getElementById('react-root'));
//
// renderApp(UserContactForm);
