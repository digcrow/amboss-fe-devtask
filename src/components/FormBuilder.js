import React from 'react';
import Field from './Field';
import styled from 'styled-components';


const P = styled.p`
    font-size: 14px;
    color: #666;
    margin: 10px 0 30px;
`;
const Container = styled.div`
  width: 100%;
  max-width: 600px;
  margin: 20px auto;
  background-color: #eaecef;
  padding: 20px 50px;
  border-radius: 10px;
  box-shadow: 0 2px 20px rgba(0,0,0,.2)
`;
const FormHeader = styled.h1`
  color: #00a2b3;
  margin-top: 5px;
`;
const Button = styled.button`
    width: calc(100% - 20px);
    padding: 8px 10px;
    margin: 10px 10px 0;
    background-color: #00a2b3;
    color: #FFF;
    border-radius: 5px;
    font-size: 14px;
    margin-bottom: 6px;
`;
const Row = styled.div`
    display: flex;
`;


class ContactForm extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            contactFormFieldIsValid :{
                firstName : false,
                lastName : false,
                areaCode : false,
                phone: false
            }
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    // updating the state of inputs validations
    handleInputChange(notValid,name,value){
        this.setState({ contactFormFieldIsValid : {
                ...this.state.contactFormFieldIsValid ,
                [name] : ! notValid
            },
            [name] : value
        });
    }

    // Handle form submission
    handleFormSubmit(e){
        e.preventDefault();
        let state = this.state.contactFormFieldIsValid;
        for (let prop in state) {
            this.refs[prop].validate();
            if( ! state[prop] ){
                return console.error('Oupsi! The form is NOTTTTT valid.');
            }
        }
        let formData = this.state;
        delete formData.contactFormFieldIsValid;
        //sending the form data
        console.info('Youpi ! The form is valid ||  Data ->:', formData)
    }

    render() {

        let form = this.props.formStructure;

        // Getting form fields.
        let formFields = [];
        for (let i = 0; i< form.fields.length; i ++){
            let tempFormFields = form.fields[i].map( (field) =>  (
                    <Field name={field.name}
                           ref={field.id}
                           inputType={field.type}
                           label={field.label}
                           required={field.required}
                           customFormGroupClass={'col ' + field.classCSS}
                           onChangeHndl={this.handleInputChange}
                           placeholder={field.placeholder}
                           inputID={field.id}
                           key={field.id}
                    />
                )
            );
            formFields.push(<Row key={i}> {tempFormFields} </Row> )
        }

        return (
            <Container className="container">
                <FormHeader>{form.name}</FormHeader>
                <P>{form.description}</P>
                <form className={"contact-form " + (form.labelInLine ? " label-inline" : "") + " :" } id={form.id} onSubmit={this.handleFormSubmit}>
                    {formFields}
                    <Button type={'submit'}>{form.submitText}</Button>
                </form>
            </Container>
        )
    }
}

export default ContactForm;