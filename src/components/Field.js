import React from 'react';
import PropTypes from 'prop-types';
import FormErrorMessage from './FormErrorMessage';
import {validationsMessages, isEmptyValidation, isNumberValidation} from '../utils/utils';
import styled from 'styled-components';


// Styling Component
//-------------------------
const InputContainer = styled.div`
    margin-bottom: 10px;
    flex: 1 100%;
`;
const Label = styled.label`
    margin-bottom: 8px;
    display: block;
    font-weight: bold;
    font-size: 14px;
    color: #585c64
    
`;
const Input = styled.input`
    width: 100% ;
    padding: 8px 10px;
    border: 2px solid #dde1e8;
    border-radius: 5px;
    font-size: 14px;
    margin-bottom: 6px;
`;


class Field extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            notValid : false,
            value : '',
            errorMessage : ''
        };
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    validate(){
        let inputValue = this.state.value;
        let msg = '';

        if(this.props.required){
            if( isEmptyValidation(inputValue) ){
                msg += validationsMessages.required;
                this.setState({
                    notValid : true,
                    errorMessage : msg
                });
                return false;
            }else{
                this.setState({
                    notValid : false,
                    errorMessage : ''
                });
            }
        }

        if( this.props.inputType === 'number' ){
            if( isNumberValidation(inputValue) ){
                msg += validationsMessages.number;
                this.setState({
                    notValid : true,
                    errorMessage : msg
                });
            }else{
                this.setState({
                    notValid : false,
                    errorMessage : ''
                });
            }
        }
        this.props.onChangeHndl( this.state.notValid, this.props.inputID, inputValue );
    }

    handleInputChange(e){
        this.setState({ value : e.target.value });
        this.validate();
    }

    render(){
        let errorMesssage = '';
        if(this.state.errorMessage !== '')
            errorMesssage = <FormErrorMessage message={this.state.errorMessage}/>;

        return(
            <InputContainer className={ this.props.customFormGroupClass + " form-group " + ( this.state.notValid === true ? 'field-error' : '' ) }>
                <Label className="control-label" htmlFor={this.props.inputID} >
                    {this.props.label + (this.props.required ? "*" : "") + " :"}
                </Label>
                <div className="input-group">
                    <Input
                        className="form-control"
                        id={this.props.inputID}
                        name={this.props.name}
                        type={'text'}
                        value={this.props.inputValue}
                        onKeyUp={this.handleInputChange}
                        onBlur={this.handleInputChange}
                        placeholder={this.props.placeholder} />
                    { errorMesssage }
                </div>
            </InputContainer>
        )
    };
}


Field.propTypes = {
    inputID: PropTypes.string,
    name: PropTypes.string.isRequired,
    inputType: PropTypes.oneOf(['text', 'number']).isRequired,
    label: PropTypes.string.isRequired,
    required: PropTypes.bool,
    onChangeHndl: PropTypes.func,
    inputValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    placeholder: PropTypes.string,
    customFormGroupClass : PropTypes.string
};

export default Field;