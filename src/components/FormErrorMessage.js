import React from 'react';
import styled from 'styled-components';

// Error message style
const FormError = styled.div`
  color: #b71229;
  font-style: italic;
  font-size: 11px;
  margin-bottom: 10px;
`;

const FormErrorMessage = props => (
    <FormError className="form-error"> {props.message} </FormError>
);

export default FormErrorMessage;