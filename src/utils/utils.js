// Error Messages
//----------------------------
export const validationsMessages = {
    required : "This is required field",
    number : "Only numeric characters allowed"
};

// Required validation function
//----------------------------
export const isEmptyValidation = value => {
    return value && value.toString().length ? false : true;
};

// Number validation function
//----------------------------
export const isNumberValidation = value => {
    return isNaN( value ) ;
};

