export const formStructure = {
    "name"  : "User Contact Form 2 by line",
    "id"    : "contactForm",
    "description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam blanditiis eligendi eos expedita natus reprehenderit tenetur voluptas.",
    "labelInLine" : false,
    "fields" : [
        [{
            "name" : "first_name",
            "id" : "firstName",
            "type": "text",
            "label" : "First Name",
            "required" : true,
            "placeholder" : "First Name",
            "classCSS" : ""
        },
            {
                "name" : "last_name",
                "id" : "lastName",
                "type": "text",
                "label" : "Last Name",
                "required" : true,
                "placeholder" : "Last Name",
                "classCSS" : ""
            }],
        [{
            "name" : "area_code",
            "id" : "areaCode",
            "type": "number",
            "label" : "Area code",
            "required" : false,
            "placeholder" : "Area code",
            "classCSS" : ""
        },
            {
                "name" : "phone",
                "id" : "phone",
                "type": "number",
                "label" : "Phone Number",
                "required" : true,
                "placeholder" : "Phone Number",
                "classCSS" : ""
            }],

    ],
    "submitText" : "Send message"
};

export const formStructure2 = {
    "name"  : "User Contact Form inline",
    "id"    : "contactForm",
    "description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam blanditiis eligendi eos expedita natus reprehenderit tenetur voluptas.",
    "labelInLine" : true,
    "fields" : [
        [{
            "name" : "first_name",
            "id" : "firstName",
            "type": "text",
            "label" : "First Name",
            "required" : true,
            "placeholder" : "First Name",
            "classCSS" : ""
        }],
        [{
            "name" : "last_name",
            "id" : "lastName",
            "type": "text",
            "label" : "Last Name",
            "required" : true,
            "placeholder" : "Last Name",
            "classCSS" : ""
        }],
        [{
            "name" : "area_code",
            "id" : "areaCode",
            "type": "number",
            "label" : "Area code",
            "required" : false,
            "placeholder" : "Area code",
            "classCSS" : ""
        }],
        [{
            "name" : "phone",
            "id" : "phone",
            "type": "number",
            "label" : "Phone Number",
            "required" : true,
            "placeholder" : "Phone Number",
            "classCSS" : ""
        }],

    ],
    "submitText" : "Send message"
};