// your form instance goes here
// import your form components from the 'components' file here
import React from 'react';
import styled from 'styled-components';
import FormBuilder from '../components/FormBuilder';
import ambossLogo from '../assets/images/amboss-logo.jpeg';

import {formStructure2}  from '../utils/forms';

// example of styled component

const CenterImage = styled.div`
  text-align: center;
  padding-top: 20px;
`;

class UserContactForm extends React.Component {
    render () {
        return (
            <div>
                <CenterImage><img src={ambossLogo} className="App-logo" alt="Amboss logo" width={200} /></CenterImage>
                <FormBuilder formStructure={formStructure2}/>
            </div>
        )
    }
}

export default UserContactForm